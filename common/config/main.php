<?php
\Yii::$container->set('pistol88\order\interfaces\Cart', 'pistol88\order\drivers\Pistol88Cart');
return [
    'name' => 'Чисто начисто',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'bootstrap' => [
        'dektrium\user\Bootstrap',
        'dektrium\rbac\Bootstrap',
    ],

    'extensions' => yii\helpers\ArrayHelper::merge(
        require( dirname(dirname(__DIR__)) . '/vendor/yiisoft/extensions.php'),
        [
            'dektrium/yii2-rbac' => [
              'name' => 'dektrium/yii2-rbac',
              'version' => '1.0.0.0-alpha',
              'alias' => [
                '@dektrium/rbac' => '@dektrium/rbac',
              ],
            ],
        ]
    ),

    'components' => [
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages', //
                    'sourceLanguage' => 'ru',
                    'fileMap' => [
                    ],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
            'itemTable' => '{{%rbac_auth_item}}',
            'itemChildTable' => '{{%rbac_auth_item_child}}',
            'assignmentTable' => '{{%rbac_auth_assignment}}',
            'ruleTable' => '{{%rbac_auth_rule}}'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            // 'clients' => [
            //     'google' => [
            //         'class' => 'yii\authclient\clients\Google',
            //         'clientId' => 'google_client_id',
            //         'clientSecret' => 'google_client_secret',
            //     ],
            //     'facebook' => [
            //         'class' => 'yii\authclient\clients\Facebook',
            //         'clientId' => 'facebook_client_id',
            //         'clientSecret' => 'facebook_client_secret',
            //     ],
            // ],
        ],
        'cart' => [
            'class' => 'pistol88\cart\Cart',
            'currency' => 'руб.', //Валюта
            'currencyPosition' => 'after', //after или before (позиция значка валюты относительно цены)
            'priceFormat' => [2,'.', ''], //Форма цены
        ],
        'client' => [
            'class' => 'pistol88\client\Client',
        ],
        'fileStorage' => [
            'class' => '\trntv\filekit\Storage',
            'baseUrl' => '@storageUrl/source',
            'filesystem'=> function() {
                $adapter = new \League\Flysystem\Adapter\Local(dirname(dirname(__DIR__)).'/frontend/web/images/source');
                return new League\Flysystem\Filesystem($adapter);
            },
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
        ],
        'gallery' => [
            'class' => 'pistol88\gallery\Module',
            'imagesStorePath' => dirname(dirname(__DIR__)).'/storage/web/images/store', //path to origin images
            'imagesCachePath' => dirname(dirname(__DIR__)).'/storage/web/images/cache', //path to resized copies
            'graphicsLibrary' => 'GD',
            'placeHolderPath' => dirname(dirname(__DIR__)).'/storage/web/images/placeHolder.png',
        ],
        'order' => [
            'class' => 'pistol88\order\Module',
            'layoutPath' => 'frontend\views\layouts',
            'successUrl' => '/page/thanks', //Страница, куда попадает пользователь после успешного заказа
            //'ordersEmail' => 'test@yandex.ru', //Мыло для отправки заказов
        ],
        'cart' => [
            'class' => 'pistol88\cart\Module',
        ],
        'promocode' => [
            'class' => 'pistol88\promocode\Module',
            'informer' => 'pistol88\cart\widgets\CartInformer', // namespace to custom cartInformer widget
            'informerSettings' => [], //settings for custom cartInformer widget
            'usesModel' => 'dektrium\user\models\User', //Модель пользователей
            //Указываем модели, к которым будем привязывать промокод
            'targetModelList' => [
                'Категории' => [
                    'model' => 'pistol88\service\models\Category',
                    'searchModel' => 'pistol88\service\models\category\CategorySearch'
                ],
                'Продукты' => [
                    'model' => 'pistol88\shop\models\Product',
                    'searchModel' => 'pistol88\shop\models\product\ProductSearch'
                ],
            ],
        ],
        'shop' => [
            'class' => 'pistol88\shop\Module',
            'adminRoles' => ['superadmin']
        ],
        'filter' => [
            'class' => 'pistol88\filter\Module',
            'adminRoles' => ['superadmin'],
            'relationFieldName' => 'category_id',
            'relationFieldValues' =>
                function() {
                    return \pistol88\shop\models\Category::buildTextTree();
                },
        ],
        'field' => [
            'class' => 'pistol88\field\Module',
            'relationModels' => [
                'pistol88\shop\models\Product' => 'Продукты',
                'pistol88\shop\models\Category' => 'Категории',
                'pistol88\shop\models\Producer' => 'Производители',
            ],
            'adminRoles' => ['superadmin'],
        ],
        'relations' => [
            'class' => 'pistol88\relations\Module',
            'fields' => ['code'],
        ],
        'client' => [
            'class' => 'pistol88\client\Module',
            'adminRoles' => ['superadmin'],
        ],
        'review' => [
            'class' => 'pistol88\review\Module',
        ],
    ],
];
