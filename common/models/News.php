<?php

namespace common\models;

use Yii;
/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title
 * @property string $anons
 * @property string $text
 * @property string $slug
 * @property string $date
 * @property string $status
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'anons', 'text'], 'required'],
            [['text', 'status'], 'string'],
            [['date'], 'safe'],
            [['title', 'slug'], 'string', 'max' => 55],
            [['anons'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'anons' => 'Анонс',
            'text' => 'Текст',
            'slug' => 'Seo-заголовок',
            'date' => 'Дата',
            'status' => 'Статус',
        ];
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                // optional params
                'ensureUnique' => true,
                'replacement' => '_',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
        ];
    }


}
