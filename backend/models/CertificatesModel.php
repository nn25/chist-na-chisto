<?php

namespace backend\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "certificates".
 *
 * @property int $id
 * @property string $description
 * @property string $image
 * @property string $url
 */
class CertificatesModel extends \yii\db\ActiveRecord
{
    public $image_file, $document_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string'],
            [['image', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Описание',
            'image' => 'Изображение',
            'url' => 'Url',
        ];
    }

    public static function get_all()
    {
        return self::find()->all();
    }

    public function save_certificate()
    {
        $image = $this->uploadImage();
        if ($image == 'redirect') return 'redirect';
        elseif ($image) $this->image = $image;

        $doc = $this->uploadDoc();
        if ($doc == 'redirect') return 'redirect';
        elseif ($doc) $this->url = $doc;

        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

    public static function findModel($id)
    {
        if (($model = self::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function uploadImage()
    {
        if($_FILES) {
            if($_FILES['CertificatesModel']['size']['image_file']) {
                $extensions = ['image/jpeg', 'image/jpg', 'image/png'];
                if(!in_array($_FILES['CertificatesModel']['type']['image_file'], $extensions)) {
                    return 'redirect';
                } else {
                    $path = '/frontend/web/image/sertificat/';
                    $i = 1;
                    while (file_exists($_SERVER["DOCUMENT_ROOT"].$path.$i.'.'.pathinfo($_FILES['CertificatesModel']['name']['image_file'], PATHINFO_EXTENSION))) $i++;
                    $fileName = $i.'.'.pathinfo($_FILES['CertificatesModel']['name']['image_file'], PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['CertificatesModel']['tmp_name']['image_file'], $_SERVER["DOCUMENT_ROOT"].$path.$fileName);
                    $otvet = $path.$fileName;
                }
            } else {
                $otvet = false;
            }
        }
        else $otvet = false;
        return $otvet;
    }

    protected function uploadDoc()
    {
        if($_FILES) {
            if($_FILES['CertificatesModel']['size']['document_file']) {
                $path = '/frontend/web/image/sertificat/';
                $i = 1;
                while (file_exists($_SERVER["DOCUMENT_ROOT"].$path.$i.'.'.pathinfo($_FILES['CertificatesModel']['name']['document_file'], PATHINFO_EXTENSION))) $i++;
                $fileName = $i.'.'.pathinfo($_FILES['CertificatesModel']['name']['document_file'], PATHINFO_EXTENSION);
                move_uploaded_file($_FILES['CertificatesModel']['tmp_name']['document_file'], $_SERVER["DOCUMENT_ROOT"].$path.$fileName);
                $otvet = $path.$fileName;
            } else {
                $otvet = false;
            }
        }
        else $otvet = false;
        return $otvet;
    }

}
