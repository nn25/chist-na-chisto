<?php

namespace backend\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "photo_and_video".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $url
 * @property int $type 0 - картинка, 1 - видос
 */
class PhotoAndVideo extends \yii\db\ActiveRecord
{
    public $image_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo_and_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'type'], 'required'],
            [['type'], 'integer'],
            [['name', 'description'], 'string', 'max' => 128],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'url' => 'Ссылка на фото или видео',
            'type' => 'Тип',
        ];
    }

    public static function get_all()
    {
        return self::find()->orderBy('type ASC')->all();
    }

    public function save_one()
    {
        $image = $this->uploadImage();
        if ($image == 'redirect') return 'redirect';
        elseif ($image) $this->url = $image;

        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

    public static function findModel($id)
    {
        if (($model = self::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return bool|string|\yii\web\Response
     */
    protected function uploadImage()
    {
        if($_FILES) {
            if($_FILES['PhotoAndVideo']['size']['image_file']) {
                $extensions = ['image/jpeg', 'image/jpg', 'image/png'];
                if(!in_array($_FILES['PhotoAndVideo']['type']['image_file'], $extensions)) {
                    return 'redirect';
                } else {
                    $path = '/frontend/web/image/foto/';
                    $i = 1;
                    while (file_exists($_SERVER["DOCUMENT_ROOT"].$path.$i.'.'.pathinfo($_FILES['PhotoAndVideo']['name']['image_file'], PATHINFO_EXTENSION))) $i++;
                    $fileName = $i.'.'.pathinfo($_FILES['PhotoAndVideo']['name']['image_file'], PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['PhotoAndVideo']['tmp_name']['image_file'], $_SERVER["DOCUMENT_ROOT"].$path.$fileName);
                    $otvet = $path.$fileName;
                }
            } else {
                $otvet = false;
            }
        }
        else $otvet = false;
        return $otvet;
    }

}
