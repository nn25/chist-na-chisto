<?php

namespace backend\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "slider_main".
 *
 * @property int $id
 * @property string $link
 * @property string $image
 * @property string $image_mini
 * @property string $text_first
 * @property string $text_second
 */
class SliderMain extends \yii\db\ActiveRecord
{
    public $image_file, $image_mini_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_main';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link', 'image', 'image_mini', 'text_first', 'text_second'], 'required'],
            [['link', 'image', 'image_mini'], 'string', 'max' => 255],
            [['text_first', 'text_second'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Ссылка',
            'image' => 'Изображение',
            'image_mini' => 'Мини изображение',
            'text_first' => 'Текст первой строки',
            'text_second' => 'Текст второй строки',
        ];
    }

    public static function get_all()
    {
        return self::find()->orderBy('id DESC')->all();
    }

    public static function get_one_by_id($id)
    {
        return self::find()->where(['id' => $id])->one();
    }

    public function save_slide()
    {
        if ($this->link==="") $this->link="#";

        $image = $this->uploadImage();
        if ($image == 'redirect') return 'redirect';
        elseif ($image) $this->image = $image;

        $image_mini = $this->uploadImageMini();
        if ($image_mini == 'redirect') return 'redirect';
        elseif ($image_mini) $this->image_mini = $image_mini;

        if ($this->save()) return true;
        else return false;
    }

    public static function findModel($id)
    {
        if (($model = self::get_one_by_id($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function uploadImage()
    {
        if($_FILES) {
            if($_FILES['SliderMain']['size']['image_file']!==0) {
                $extensions = ['image/jpeg', 'image/jpg', 'image/png'];
                if(!in_array($_FILES['SliderMain']['type']['image_file'], $extensions)) {
                    $otvet = 'redirect';
                } else {
                    $path = '/frontend/web/image/slides/';
                    $i = 1;
                    while (file_exists($_SERVER["DOCUMENT_ROOT"].$path.$i.'.'.pathinfo($_FILES['SliderMain']['name']['image_file'], PATHINFO_EXTENSION))) $i++;
                    $fileName = $i.'.'.pathinfo($_FILES['SliderMain']['name']['image_file'], PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['SliderMain']['tmp_name']['image_file'], $_SERVER["DOCUMENT_ROOT"].$path.$fileName);
                    $otvet = $path.$fileName;
                }
            } else {
                $otvet = false;
            }
        }
        else $otvet = false;
        return $otvet;
    }

    protected function uploadImageMini()
    {
        if($_FILES) {
            if($_FILES['SliderMain']['size']['image_mini_file']!==0) {
                $extensions = ['image/jpeg', 'image/jpg', 'image/png'];
                if(!in_array($_FILES['SliderMain']['type']['image_mini_file'], $extensions)) {
                    $otvet = 'redirect';
                } else {
                    $path = '/frontend/web/image/slides/';
                    $i = 1;
                    while (file_exists($_SERVER["DOCUMENT_ROOT"].$path.$i.'.'.pathinfo($_FILES['SliderMain']['name']['image_mini_file'], PATHINFO_EXTENSION))) $i++;
                    $fileName = $i.'.'.pathinfo($_FILES['SliderMain']['name']['image_mini_file'], PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['SliderMain']['tmp_name']['image_mini_file'], $_SERVER["DOCUMENT_ROOT"].$path.$fileName);
                    $otvet = $path.$fileName;
                }
            } else {
                $otvet = false;
            }
        }
        else $otvet = false;
        return $otvet;
    }

}
