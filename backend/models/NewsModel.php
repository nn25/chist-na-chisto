<?php

namespace backend\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $category
 * @property string $image
 * @property string $title
 * @property string $anons
 * @property string $text
 * @property string $slug
 * @property string $date
 * @property int $published
 */
class NewsModel extends \yii\db\ActiveRecord
{
    public $image_file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                // optional params
                'ensureUnique' => true,
                'replacement' => '_',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'title', 'anons', 'text'], 'required'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['published'], 'integer'],
            [['category'], 'string', 'max' => 128],
            [['image', 'title', 'slug'], 'string', 'max' => 255],
            [['anons'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Категория',
            'image' => 'Изображение',
            'title' => 'Заголовок',
            'anons' => 'Анонс',
            'text' => 'Текст',
            'slug' => 'ЧПУ',
            'date' => 'Date',
            'published' => 'Опубликовано?',
        ];
    }

    public static function get_all()
    {
        return self::find()->all();
    }

    public function save_news()
    {
        if ($this->date == "") $this->date = date('Y-m-d H:i:s');

        $image = $this->uploadImage();
        if ($image == 'redirect') return 'redirect';
        elseif ($image) $this->image = $image;

        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

    public static function findModel($id)
    {
        if (($model = self::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return bool|string|\yii\web\Response
     */
    protected function uploadImage()
    {
        if($_FILES) {
            if($_FILES['NewsModel']['size']['image_file']) {
                $extensions = ['image/jpeg', 'image/jpg', 'image/png'];
                if(!in_array($_FILES['NewsModel']['type']['image_file'], $extensions)) {
                    return 'redirect';
                } else {
                    $path = '/frontend/web/image/publications/';
                    $i = 1;
                    while (file_exists($_SERVER["DOCUMENT_ROOT"].$path.$i.'.'.pathinfo($_FILES['NewsModel']['name']['image_file'], PATHINFO_EXTENSION))) $i++;
                    $fileName = $i.'.'.pathinfo($_FILES['NewsModel']['name']['image_file'], PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['NewsModel']['tmp_name']['image_file'], $_SERVER["DOCUMENT_ROOT"].$path.$fileName);
                    $otvet = $path.$fileName;
                }
            } else {
                $otvet = false;
            }
        }
        else $otvet = false;
        return $otvet;
    }

}
