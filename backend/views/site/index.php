<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Магазин';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Все системы в норме!</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Витрина</h2>
                <ul>
                    <li><a href="<?=Url::toRoute(['/shop/category/index']);?>">Категории</a></li>
                    <li><a href="<?=Url::toRoute(['/shop/product/index']);?>">Товары</a></li>
                </ul>
            </div>
            <div class="col-lg-4">
                <h2>Информация</h2>
                <ul>
                    <li><a href="<?=Url::toRoute(['/page/index']);?>">Страницы</a></li>
                    <li><a href="<?=Url::toRoute(['/news/index']);?>">Новости</a></li>
                    <li><a href="<?=Url::toRoute(['/slider/index']);?>">Слайдер</a></li>
                    <li><a href="<?=Url::toRoute(['/photovideo/index']);?>">Фото и видео</a></li>
                    <li><a href="<?=Url::toRoute(['/contacts/index']);?>">Контакты</a></li>
                    <li><a href="<?=Url::toRoute(['/certificates/index']);?>">Сертификаты</a></li>
                </ul>
            </div>
            <div class="col-lg-4">
                <h2>Настройки</h2>
                <ul>
                    <li><a href="<?=Url::toRoute(['/field/field/index']);?>">Поля</a></li>
                    <li><a href="<?=Url::toRoute(['/filter/filter/index']);?>">Фильтры</a></li>
                </ul>
            </div>
        </div>

    </div>
</div>
