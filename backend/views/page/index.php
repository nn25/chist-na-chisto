<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать страницу', ['create'], ['class' => 'btn btn-success hidden']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' =>'show_page',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'show_page',
                    ['Yes' => 'Да','No' => 'Нет'],
                    ['class' => 'form-control', 'prompt' => 'Статус']
                ),
                'format' => 'raw',
                'value' => function($model){
                    $translate =['Yes' => 'Да','No' => 'Нет'];
                    return $translate[$model ->show_page];
                }
            ],
            // 'id',
            'title',
            //'text:ntext',
            //'slug',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update} ',  'buttonOptions' => ['class' => 'btn btn-default'], 'options' => ['style' => 'width: 65px;']],
        ],
    ]); ?>
</div>
<style>
    .glyphicon-trash{display: none;}
</style>
