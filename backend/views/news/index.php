<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table menu">

        <div class="row caption">
            <div class="col-md-3">Категория</div>
            <div class="col-md-4">Заголовок</div>
            <div class="col-md-3">Анонс</div>
            <div class="col-md-2"></div>
        </div>

        <?php foreach($news as $new) {?>
            <div class="row">
                <div class="col-md-3 text"><?=$new->category?></div>
                <div class="col-md-4 text"><?=$new->title?></div>
                <div class="col-md-3 text"><?=$new->anons?></div>
                <div class="col-md-2">
                    <a href="/backend/web/news/update/<?=$new->id?>"><img src="/backend/web/img/edit.png"></a>
                    <a href="/backend/web/news/delete/<?=$new->id?>" title="Удалить"><img src="/backend/web/img/trash.png"></a>
                </div>
            </div>
        <?php } ?>

    </div>

</div>
