<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить контакт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table menu">

        <div class="row caption">
            <div class="col-md-5">Тип</div>
            <div class="col-md-5">Значение</div>
            <div class="col-md-2"></div>
        </div>

        <?php foreach($contacts as $contact) {?>
            <div class="row">
                <div class="col-md-5 text"><?=$contact->property?></div>
                <div class="col-md-5 text"><?=$contact->value?></div>
                <div class="col-md-2">
                    <a href="/backend/web/contacts/update/<?=$contact->id?>"><img src="/backend/web/img/edit.png"></a>
                    <a href="/backend/web/contacts/delete/<?=$contact->id?>" title="Удалить"><img src="/backend/web/img/trash.png"></a>
                </div>
            </div>
        <?php } ?>

    </div>
</div>
