<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = 'Редактирование Контакта: ' . $model->property;
$this->params['breadcrumbs'][] = ['label' => 'Контакты', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="slider-update">

    <h1><?= Html::encode($model->property) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
