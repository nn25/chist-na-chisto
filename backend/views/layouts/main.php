<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/frontend/web/favicon.png"/>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => '<i class="glyphicon glyphicon-leaf blue" style="color: #ccc;"></i> ' . Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    if (Yii::$app->user->id) {
        $navList = [
            'options' => ['class' => 'navbar-nav navbar-right main-menu'],
            'encodeLabels' => false,
            'items' => [
                [
                    'label' => 'Сайт',
                    'url' => '',
                    'options' => ['title' => 'Управление сайтом'],
                    'items' => [
                        [
                            'label' => 'Новости',
                            'url' => ['/news'],
                            'options' => [
                                'style' => 'font-weight: bolder; color: green !important;',
                                'title' => 'Новости'],
                        ],
                        [
                            'label' => 'Слайдер',
                            'url' => ['/slider'],
                            'options' => [
                                'style' => 'font-weight: bolder; color: green !important;',
                                'title' => 'Слайдер'
                            ],
                        ],
                        [
                            'label' => 'Страницы',
                            'url' => ['/page'],
                            'options' => [
                                'style' => 'font-weight: bolder; color: green !important;',
                                'title' => 'Страницы'
                            ],
                        ],
                        [
                            'label' => 'Фото и видео',
                            'url' => ['/photovideo'],
                            'options' => [
                                'style' => 'font-weight: bolder; color: green !important;',
                                'title' => 'Фото и видео'
                            ],
                        ],
                        [
                            'label' => 'Контакты',
                            'url' => ['/contacts'],
                            'options' => [
                                'style' => 'font-weight: bolder; color: green !important;',
                                'title' => 'Контакты'
                            ],
                        ],
                        [
                            'label' => 'Сертификаты',
                            'url' => ['/certificates'],
                            'options' => [
                                'style' => 'font-weight: bolder; color: green !important;',
                                'title' => 'Сертификаты'
                            ],
                        ],
                    ],
                ],
                [
                    'label' => 'Отчёты',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Журнал заказов', 'url' => ['/order/order/index']],
                        ['label' => 'Заказы по месяцам', 'url' => ['/order/stat/index']],
                    ]
                ],
                [
                    'label' => 'Справочники',
                    'url' => '#',
                    'items' => [
//                        ['label' => 'Кассы', 'url' => ['/cashbox/cashbox/index']],
                        ['label' => 'Витрина', 'url' => ['/shop/product/index']],
                        ['label' => 'Промокоды', 'url' => ['/promocode/promo-code']],
//                        ['label' => 'Сертификаты', 'url' => ['/certificate/certificate']],
                        ['label' => 'Клиенты', 'url' => ['/client/client/index']],
//                        ['label' => 'Пользователи', 'url' => ['/user/index'],],
//                        ['label' => 'Расходники', 'url' => ['/consumption/cost/index'],],
//                        ['label' => 'Расходы', 'url' => ['/spending/spending/index'],],
                    ]
                ],
                [
                    'label' => '<i class="glyphicon glyphicon-cog"></i>',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Фильтры', 'url' => ['/filter/filter']],
                        ['label' => 'Поля', 'url' => ['/field/field']],
                    ]
                ],
                [
                    'label' => '<i class="glyphicon glyphicon-user"></i>',
                    'url' => '#',
                    'options' => ['title' => 'Профиль'],
                    'items' => [
                        ['label' => 'Изменить пароль', 'url' => ['/sign-in/account'], 'icon' => '<i class="fa fa-angle-double-right"></i>'],
                        ['label' => 'Выход', 'url' => ['/sign-in/logout'], 'linkOptions' => ['data-method' => 'post'], 'icon' => '<i class="fa fa-angle-double-right"></i>'],
                    ]
                ],
                ['label' => yii::$app->user->getIdentity()->username, 'url' => ['/sign-in/profile'], 'icon' => '<i class="fa fa-angle-double-right"></i>'],
            ]
        ];
        echo Nav::widget($navList);
    } else {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'Вход',
                    'url' => '/backend/web/app/login',
                ],
            ]
        ]);
    }

    NavBar::end();
    ?>

    <div class="container main-container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?php echo Alert::widget();?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"></p>

        <p class="pull-right">&copy; <a href="http://dvizh.net/" target="_blank">Dvizh software</a> <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
<?=$this->registerJs("
    $('.text').click(function(){
        if($(this).hasClass('active')) $(this).removeClass('active');
        else $(this).addClass('active');
    });
",yii\web\View::POS_READY);?>
</html>
<?php $this->endPage() ?>
