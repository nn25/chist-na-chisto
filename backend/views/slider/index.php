<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдер';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить слайд', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table menu">

        <div class="row caption">
            <div class="col-md-4"></div>
            <div class="col-md-3">Текст 1 строки</div>
            <div class="col-md-3">Текст 2 строки</div>
            <div class="col-md-2"></div>
        </div>

        <?php foreach($slider as $slide) {?>
            <div class="row">
                <div class="col-md-4 imgs"><img src="<?=$slide->image?>"></div>
                <div class="col-md-3 text"><?=$slide->text_first?></div>
                <div class="col-md-3 text"><?=$slide->text_second?></div>
                <div class="col-md-2">
                    <a href="/backend/web/slider/update/<?=$slide->id?>"><img src="/backend/web/img/edit.png"></a>
                    <a href="/backend/web/slider/delete/<?=$slide->id?>" title="Удалить"><img src="/backend/web/img/trash.png"></a>
                </div>
            </div>
        <?php } ?>

    </div>
</div>
