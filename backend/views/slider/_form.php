<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="col-md-6">
        <?= $form->field($model, 'text_first')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'text_second')->textInput() ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'link')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?php if($model->image) echo '<img src="'.$model->image.'" style="max-height:60px;max-width:200px;">';?>
        <?= $form->field($model, 'image_file')->fileInput()->label('Изменить фото (jpg, jpeg, png)') ?>
    </div>

    <div class="col-md-6">
        <?php if($model->image_mini) echo '<img src="'.$model->image_mini.'" style="max-height:60px;max-width:200px;">';?>
        <?= $form->field($model, 'image_mini_file')->fileInput()->label('Изменить иконку (jpg, jpeg, png)') ?>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
