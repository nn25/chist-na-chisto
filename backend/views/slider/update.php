<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = 'Редактирование Слайда: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="slider-update">

    <h1><?= Html::encode($model->text_first) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
