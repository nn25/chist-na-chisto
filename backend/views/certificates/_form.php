<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
        <?= $form->field($model, 'description')->textarea();?>
    </div>

    <div class="col-md-12">
        <?php if($model->image) echo '<img src="'.$model->image.'" style="max-height:60px;max-width:200px;">';?>
        <?= $form->field($model, 'image_file')->fileInput()->label('Изображение (jpg, jpeg, png)') ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'document_file')->fileInput()->label('Документ (pdf, doc, docx)') ?>
        <?php if($model->url) echo '<a href="'.$model->url.'" target="_blank">Текущий документ</a><br>';?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
