<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сертификаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить сертификат', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table menu">

        <div class="row caption">
            <div class="col-md-3">Изображение</div>
            <div class="col-md-7">Описание</div>
            <div class="col-md-2"></div>
        </div>

        <?php foreach($certificates as $certificate) {?>
            <div class="row">
                <div class="col-md-3 imgs"><img src="<?=$certificate->image?>"></div>
                <div class="col-md-7 text"><?=$certificate->description?></div>
                <div class="col-md-2">
                    <a href="/backend/web/certificates/update/<?=$certificate->id?>"><img src="/backend/web/img/edit.png"></a>
                    <a href="/backend/web/certificates/delete/<?=$certificate->id?>" title="Удалить"><img src="/backend/web/img/trash.png"></a>
                </div>
            </div>
        <?php } ?>

    </div>

</div>
