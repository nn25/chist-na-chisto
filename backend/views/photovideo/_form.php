<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="photovideo-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
        <?= $form->field($model, 'type')->dropDownList([
            '0' => 'Фото',
            '1' => 'Видео'
        ]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-12">
        <?php if($model->url and $model->type==0) echo '<img src="'.$model->url.'" style="max-height:60px;max-width:200px;">';?>
        <?= $form->field($model, 'image_file')->fileInput()->label('Прикрепить изображение') ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
