<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фото и видео';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить фото/видео', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table menu">

        <div class="row caption">
            <div class="col-md-3"></div>
            <div class="col-md-5">Описание</div>
            <div class="col-md-2">Тип</div>
            <div class="col-md-2"></div>
        </div>

        <?php foreach($photoAndVideo as $pav) {?>
            <div class="row">
                <div class="col-md-3 imgs"><?=($pav->type==0)?'<img src="'.$pav->url.'">':'<img src="/frontend/web/image/video.png">'?></div>
                <div class="col-md-5 text"><?=$pav->description?></div>
                <div class="col-md-2 text"><?=($pav->type==0)?'Фото':'Видео'?></div>
                <div class="col-md-2">
                    <a href="/backend/web/photovideo/update/<?=$pav->id?>"><img src="/backend/web/img/edit.png"></a>
                    <a href="/backend/web/photovideo/delete/<?=$pav->id?>" title="Удалить"><img src="/backend/web/img/trash.png"></a>
                </div>
            </div>
        <?php } ?>

    </div>

</div>
