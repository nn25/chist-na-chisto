<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = 'Редактор: '.($model->type==0)?'Фото':'Видео';
$this->params['breadcrumbs'][] = ['label' => 'Фото/видео', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="news-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
