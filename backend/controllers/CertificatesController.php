<?php

namespace backend\controllers;

use backend\models\CertificatesModel;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * CertificatesController implements the CRUD actions for CertificatesModel model.
 */
class CertificatesController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index','create','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $certificates = CertificatesModel::get_all();
        return $this->render('index', ['certificates' => $certificates]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CertificatesModel();

        if ($model->load(Yii::$app->request->post())) {
            $save = $model->save_certificate();
            if ($save === 'redirect') {
                Yii::$app->session->setFlash('error', 'Неверный формат файла');
                return $this->redirect(['create']);
            } elseif ($save) {
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось произвести сохранение');
                return $this->redirect(['create']);
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = CertificatesModel::findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $save = $model->save_certificate();
            if ($save === 'redirect') {
                Yii::$app->session->setFlash('error', 'Неверный формат файла');
                return $this->redirect(['/news/update/'.$id]);
            } elseif ($save) {
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось произвести сохранение');
                return $this->redirect(['/news/update/'.$id]);
            }
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        CertificatesModel::findModel($id)->delete();
        return $this->redirect(['index']);
    }

}
