<?php

namespace backend\controllers;

use Yii;
use common\models\Slider;
use common\models\search\SliderSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use backend\models\SliderMain;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index','create','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $slider = SliderMain::get_all();
        return $this->render('index', [
            'slider' => $slider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => SliderMain::findModel($id),
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SliderMain();

        if ($model->load(Yii::$app->request->post())) {
            $save = $model->save_slide();
            if ($save === 'redirect') {
                Yii::$app->session->setFlash('error', 'Неверный формат файла');
                return $this->redirect('/backend/web/slider/create');
            } elseif ($save) {
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось произвести сохранение');
                return $this->redirect('/backend/web/slider/create');
            }
        }

        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = SliderMain::findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $save = $model->save_slide();
            if ($save === 'redirect') {
                Yii::$app->session->setFlash('error', 'Неверный формат файла');
                return $this->redirect('/backend/web/slider/update/'.$id);
            } elseif ($save) {
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось произвести сохранение');
                return $this->redirect('/backend/web/slider/update/'.$id);
            }
        }

        return $this->render('update', ['model' => $model,]);
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        SliderMain::findModel($id)->delete();
        return $this->redirect(['index']);
    }

}
