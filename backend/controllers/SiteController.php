<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'create-order','image-upload','images-get'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/storage/web/images/store/news', // Directory URL address, where files are stored.
                'path' => '@imagestore/news' // Or absolute path to directory where files are stored.
            ],
             'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/storage/web/images/store/news', // Directory URL address, where files are stored.
                'path' => '@imagestore/news', // Or absolute path to directory where files are stored.
                //'type' => GetAction::TYPE_IMAGES,
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //return $this->redirect('/backend/web/order/order/create');
        
        return $this->render('index');
    }

}
