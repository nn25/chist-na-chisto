<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $category
 * @property string $image
 * @property string $title
 * @property string $anons
 * @property string $text
 * @property string $slug
 * @property string $date
 * @property int $published
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'image', 'title', 'anons', 'text', 'slug', 'date'], 'required'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['published'], 'integer'],
            [['category'], 'string', 'max' => 128],
            [['image', 'title', 'slug'], 'string', 'max' => 255],
            [['anons'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Категория',
            'image' => 'Изображение',
            'title' => 'Заголовок',
            'anons' => 'Анонс',
            'text' => 'Описание',
            'slug' => 'ЧПУ',
            'date' => 'Date',
            'published' => 'Опубликовано?',
        ];
    }

    public static function get_all()
    {
        return self::find()->where(['published' => 1])->orderBy('id DESC')->all();
    }

    public static function get_last_news($count)
    {
        return self::find()->where(['published' => 1])->orderBy('id DESC')->limit($count)->all();
    }

    public static function get_news_by_category($category)
    {
        return self::find()->where(['category' => $category, 'published' => 1])->all();
    }

    public static function get_news_by_slug($slug)
    {
        return self::find()->where(['slug' => $slug])->one();
    }

    public static function get_random_news()
    {
        return self::find()->where(['published' => 1])->orderBy('RAND()')->one();
    }

}
