<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slider_main".
 *
 * @property int $id
 * @property string $link
 * @property string $image
 * @property string $image_mini
 * @property string $text_first
 * @property string $text_second
 */
class SliderMain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_main';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link', 'image', 'image_mini', 'text_first', 'text_second'], 'required'],
            [['link', 'image', 'image_mini'], 'string', 'max' => 255],
            [['text_first', 'text_second'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Ссылка',
            'image' => 'Изображение',
            'image_mini' => 'Мини изображение',
            'text_first' => 'Текст первой строки',
            'text_second' => 'Текст второй строки',
        ];
    }

    public static function get_all()
    {
        return self::find()->orderBy('id DESC')->all();
    }

}
