<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $body;
    public $city;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['city', 'body'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'email' => 'Email или телефон',
            'city' => 'Город',
            'body' => 'Сообщение',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom(['ooochistonachisto@gmail.com' => 'Чисто начисто'])
            ->setSubject('Письмо с сайта Чисто начисто')
            ->setHtmlBody($this->name.
                          '<br>'.
                          $this->email.
                          '<br>'.
                          $this->city.
                          '<br><br>'.
                          $this->body)
            ->send();
    }
}
