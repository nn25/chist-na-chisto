<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "certificates".
 *
 * @property int $id
 * @property string $description
 * @property string $image
 * @property string $url
 */
class Certificates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'image', 'url'], 'required'],
            [['description'], 'string'],
            [['image', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'image' => 'Image',
            'url' => 'Url',
        ];
    }

    public static function get_all()
    {
        return self::find()->all();
    }
}
