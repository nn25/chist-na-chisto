<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $property
 * @property string $value
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property', 'value'], 'required'],
            [['value'], 'string'],
            [['property'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property' => 'Property',
            'value' => 'Value',
        ];
    }

    public static function get_all()
    {
        return self::find()->all();
    }

    public static function get_title()
    {
        $title = self::find()->where(['property' => 'Заголовок'])->one();
        return $title->value;
    }

}
