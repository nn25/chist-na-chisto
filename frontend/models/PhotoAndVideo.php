<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "photo_and_video".
 *
 * @property int $id
 * @property int $name
 * @property int $description
 * @property int $url
 * @property int $type 0 - картинка, 1 - видос
 */
class PhotoAndVideo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo_and_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'url', 'type'], 'required'],
            [['name', 'description', 'url'], 'string'],
            ['type','integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'url' => 'Url',
            'type' => 'Type',
        ];
    }

    public static function get_all()
    {
        return self::find()->all();
    }

}
