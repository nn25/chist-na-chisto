<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property string $show_page
 * @property string $slug
 * @property string $title
 * @property string $text
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['show_page', 'text'], 'string'],
            [['slug', 'title', 'text'], 'required'],
            [['slug', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'show_page' => 'Show Page',
            'slug' => 'Slug',
            'title' => 'Title',
            'text' => 'Text',
        ];
    }

    public static function get_page_by_slug($slug)
    {
        return self::find()->where(['slug' => $slug])->one();
    }

}
