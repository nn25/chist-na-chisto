<?php

namespace app\models;

use Yii;

class Search extends \yii\db\ActiveRecord
{
    public $search;
    public $keyWords;
    public $sku;
    public $minPrice;
    public $maxPrice;

    public function rules()
    {
        return [
            [['search', 'keyWords', 'sku', 'minPrice', 'maxPrice'], 'string'],
        ];
    }
}
