<?php
/* @var $this yii\web\View */

use pistol88\cart\widgets\BuyButton;

$this->title = 'Продукт';
?>

<div class="container">

    <ul class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li><a href="/category/list">Продукция</a></li>
        <li><a href="/category/page/<?=$product->category->slug?>"><?=$product->category->name?></a></li>
        <li class="active"><?=$product->name?></li>
    </ul>

    <div class="product-page">
        <div class="row">
            <div class="col-sm-3">
                <div class="item-image">
                    <div class="main-image-box">
                        <a href="/backend/web<?=$product->getImage()->getUrl()?>" data-fancybox>
                            <img id="main-img" src="/backend/web<?=$product->getImage()->getUrl()?>" data-zoom-image="/backend/web<?=$product->getImage()->getUrl()?>"/>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <div id="product-gallery">
                        <?php foreach($product->getImages() as $image) { ?>
                            <a href="#" data-image="/backend/web<?=$image->getUrl()?>" data-zoom-image="/backend/web<?=$image->getUrl()?>" class="active">
                                <img id="main-img" src="/backend/web<?=$image->getUrl()?>" />
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <h1><?=$product->name?></h1>
                <form>
                    <div class="row">
                        <div class="col-sm-6">
                            <?php echo $product->code ? 'арт. <b>'.$product->code.'</b>' : ''?>
                        </div>
                        <div class="col-sm-6">
                            <div class="count">
                                <div class="pull-right"> шт.</div>
                                <a class="plus" href="#">+</a>
                                <input class="quantity num" value="1" type="text">
                                <a class="minus" href="#">-</a>
                                <div class="clearbox"></div>
                                <div class="price"><?php echo $product->price ? $product->price.' руб.' : ''?></div>
                            </div>
<!--                            <span class="weight">вес</span>-->
                        </div>
                    </div>
                    <div class="text-right">
                        <?= BuyButton::widget([
                            'model' => $product,
                            'text' => '<i class="fa fa-shopping-cart" aria-hidden="true"></i> Добавить в корзину',
                            'htmlTag' => 'button',
                            'cssClass' => 'bttn'
                        ]) ?>
<!--                        <button class="bttn"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Добавить в корзину</button>-->
                    </div>
                </form>
            </div>
            <div class="col-sm-12">
                <?=$product->text?>
            </div>
        </div>
    </div>

</div>

<div class="container-fluid recommend">
    <div class="block-title"><span>Рекомендованные товары</span></div>
    <div class="container">
        <div class="row">
            <?php foreach($recommended as $rec) { ?>
                <div class="col-sm-3">
                    <div class="item">
                        <div class="item-category"><?=$rec->category->name?></div>
                        <div class="image-block" style="background-image:url(/backend/web<?=$rec->getImage()->getUrl()?>);"></div>
                        <a href="/category/product/<?=$rec->slug?>" class="title"><?=$rec->getName()?></a>
                        <div class="desc"><?=$rec->text?></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
