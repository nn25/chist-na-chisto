<?php
/* @var $this yii\web\View */

use pistol88\cart\widgets\BuyButton;
use pistol88\shop\models\Product;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

$this->title = 'Категория';
?>

<div class="container">

    <ul class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li><a href="/category/list">Продукция</a></li>
        <li class="active"><?=$category->name?></li>
    </ul>

    <h1><?=$category->name?></h1>

    <div class="row category-view">
        <div class="col-sm-9">

<?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_product_list',
        'layout' => "<div class='pagination-panel'>{pager}<span>{summary}</span></div>\n
                    <div class='category-title'>".$category->name."</div>
                    {items}\n
                    <div class='pagination-panel'>{pager}<span>{summary}</span></div>",
        'summary' => '{begin} - {end} из {totalCount} позиций',
        'emptyText' => 'Не найдено товаров',
        'pager' => [
            'prevPageLabel' => '< Назад',
            'nextPageLabel' => 'Вперед >',
            'maxButtonCount' => 5,
        ],
    ]);
?>

            <div class="seo-text">
                <?=$news->anons?>
            </div>
            <a href="/advertisment/page/<?=$news->slug?>" class="return">Читать далее</a>

        </div>
        <div class="col-sm-3 sidebar">
            <?=$this->context->renderPartial('_search');?>

            <div class="side-block">
                <p><b>Перейти в категорию</b></p>
                <ul class="category-navigation">
                    <?php foreach($categories as $categ){?>
                        <li>
                            <a href="/category/list/<?=$categ['slug']?>"><?=$categ['name']?></a>
                            <ul>
                                <?php foreach($categ['childs'] as $cat) { ?>
                                    <li>
                                        <a href="/category/page/<?=$cat['slug']?>"><?=$cat['name']?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>

                </ul>
            </div>

        </div>
    </div>

</div>

<?=$this->registerJs("
    $(document).ready(function(){
        $('#search-minprice').attr('placeholder', 'от');
        $('#search-maxprice').attr('placeholder', 'до');
    });
",yii\web\View::POS_READY);?>