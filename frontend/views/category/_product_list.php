<?php

use pistol88\cart\widgets\BuyButton;
use pistol88\cart\widgets\ChangeCount;

?>

<div class="row">
    <a href="/category/product/<?=$model->slug?>" class="col-sm-3"><img src="/backend/web/<?=$model->getImage()->getUrl()?>" alt=""></a>
    <a href="/category/product/<?=$model->slug?>" class="h3 col-sm-9"><?=$model->name?></a>
    <div class="col-sm-6">
        <div class="pull-right"><?php echo $model->code ? 'арт. <b>'.$model->code.'</b>' : ''?></div>
        <?=$model->short_text?>
    </div>
    <div class="col-sm-3">
        <div class="clearfix price">
            <?php echo $model->price ? $model->price.' руб.' : ''?>
        </div>
        <form class="count">
            <?=ChangeCount::widget([
                'model' => $model,
                'downArr' => '-',
                'upArr' => '+'
            ]);?>
            <div class="clearbox"></div>
            <?= BuyButton::widget([
                'model' => $model,
                'text' => '<i class="fa fa-shopping-cart" aria-hidden="true"></i> Добавить в корзину',
                'htmlTag' => 'button',
                'cssClass' => 'bttn'
            ]) ?>
        </form>
    </div>
</div>