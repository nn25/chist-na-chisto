<?php
use yii\widgets\ActiveForm;
use app\models\Search;
?>

<?php $model = new Search;?>
<?php $form = ActiveForm::begin([
    'action' => '',
    'options'=>['class' => 'search-product'],
]); ?>

<p><b>Поиск по категории</b></p>
<label>Ключевые слова</label>
<?= $form->field($model, 'keyWords')->label(false)->textInput()?>

<label>Артикул</label>
<?= $form->field($model, 'sku')->label(false)->textInput()?>

<label>Розничная</label>
<?= $form->field($model, 'minPrice')->label(false)->textInput(['class' => 'minInput'])?>
<?= $form->field($model, 'maxPrice')->label(false)->textInput(['class' => 'minInput'])?>

<button class="bttn" type="submit">Показать</button>
<?php ActiveForm::end(); ?>