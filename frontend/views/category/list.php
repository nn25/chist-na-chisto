<?php
/* @var $this yii\web\View */

$this->title = 'Список категорий';
?>

<div class="container">

    <ul class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li class="active">Продукция</li>
    </ul>

    <h1>Продукция</h1>

    <div class="row category-list">
        <?php foreach($categories as $category) { ?>
            <?php
                if ($category->parent_id == NULL) $href = '/category/list/'.$category->slug;
                else $href = '/category/page/'.$category->slug;
            ?>
            <div class="col-sm-3">
                <a href="<?=$href?>"><img src="/backend/web<?=$category->getImage()->getUrl()?>" alt=""></a>
                <a href="<?=$href?>" class="category-name"><?=$category->name?></a>
                <div class="desc"><?=$category->text?></div>
                <a href="<?=$href?>" class="show-products"><span>Показать продукцию</span> ></a>
            </div>
        <?php } ?>
    </div>

</div>