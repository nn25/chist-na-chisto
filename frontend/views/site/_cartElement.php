<?php

use pistol88\cart\widgets\ChangeCount;
use pistol88\cart\widgets\DeleteButton;
use yii\helpers\Url;
?>

<div class="clearfix cart-row">
    <div class="col-sm-2">
        <img src="/backend/web/<?=$model->getModel()->getImage()->getUrl()?>" alt="">
    </div>
    <div class="col-sm-5">
        <p><a href="<?=Url::to(['category/product', 'slug' => $model->getModel()->slug])?>"><?=$model->getModel()->name?></a></p>
        <p><?php echo $model->getModel()->code ? 'арт. <b>'.$model->getModel()->code.'</b>' : ''?></p>
    </div>
    <div class="col-sm-2"></div>
    <div class="col-sm-2">
            <?=ChangeCount::widget([
                'model' => $model,
                'downArr' => '-',
                'upArr' => '+'
            ]);?>
    </div>
    <div class="col-sm-1">
        <?=DeleteButton::widget([
            'model' => $model,
            'cssClass' => 'bttn-grey'
        ]);?>
    </div>
</div>