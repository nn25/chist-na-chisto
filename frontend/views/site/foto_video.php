<?php

/* @var $this yii\web\View */

$this->title = 'Фото и видео';
?>

<div class="container">

    <ul class="breadcrumb">
        <li><a href="index.php">Главная</a></li>
        <li class="active">Фото и видео</li>
    </ul>

    <h1>Фото и видео</h1>

    <div class="foto-box">
        <p class="h3"><span>Фото</span></p>
        <div class="row">

            <?php foreach($content as $con) { ?>
                <?php if($con->type==0) { ?>
                <div class="col-sm-3">
                    <div>
                        <a href="<?=$con->url?>" data-fancybox="group" data-caption="<?=$con->description?>">
                            <img src="<?=$con->url?>" alt="">
                        </a>
                    </div>
                </div>
                <?php } ?>
            <?php } ?>

        </div>
    </div>

    <div class="foto-box">
        <p class="h3"><span>Видео</span></p>
        <div class="row">

            <?php foreach($content as $con) { ?>
                <?php if($con->type==1) { ?>
                    <div class="col-sm-3">
                        <div class="video">
                            <a data-fancybox href="<?=$con->url?>" data-caption="<?=$con->description?>"></a>
                            <p><?=$con->name?></p>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

        </div>
    </div>

</div>