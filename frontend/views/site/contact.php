<?php

/* @var $this yii\web\View */

$this->title = 'Контакты';
?>

<div class="container">

    <ul class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li class="active">Контакты</li>
    </ul>

    <h1>Контакты компании</h1>

    <div class="contact-box">
        <div class="clearfix">
            <div class="col-sm-2">
                <img src="/frontend/web/image/logo.png" alt="">
            </div>
            <div class="col-sm-10">
                <p class="green h4"><?=$title?></p>
                <?php foreach($contacts as $contact) { ?>
                    <?php switch($contact->property){
                        case 'Телефон': echo'<p><i class="fa fa-phone" aria-hidden="true"></i> '.$contact->value.'</p>';
                                        break;
                        case 'Skype': echo'<p><i class="fa fa-skype" aria-hidden="true"></i> '.$contact->value.'</p>';
                                      break;
                        case 'Email': echo'<p><i class="fa fa-envelope-o" aria-hidden="true"></i> '.$contact->value.'</p>';
                                      break;
                        case 'Часы работы': echo'<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.$contact->value.'</p>';
                                            break;
                        case 'Заголовок': break;
                        default: echo'<p><b>'.$contact->property.':</b> '.$contact->value.'</p>';
                                 break;
                    }
                    ?>
                <?php } ?>
                <a href="/frontend/web/image/scheme_drive.jpg" data-fancybox data-caption="Схема проезда" class="bttn">Схема проезда</a>
            </div>
        </div>
        <div class="map">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Af64534fea8e1f73043f61d81ba3447793d3b18e1e01a6e02ada777a4a57f01ac&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=false"></script>
        </div>
    </div>

</div>
