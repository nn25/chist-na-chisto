<?php

/* @var $this yii\web\View */

$this->title = 'Общая информация';
?>

<div class="container">

<ul class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li class="active"><?=$content->title?></li>
</ul>

<h1><?=$content->title?></h1>

<div class="row">
    <div class="col-sm-9">
        <div class="static-content">
            <?=$content->text?>
<!--            <a href="#" class="bttn">Оформление кнопки</a>-->
        </div>
    </div>
    <div class="col-sm-3">
        <ul class="sidebar-menu">
            <li class="active"><a href="/site/page/obshchaya_informatsiya">Общая информация</a></li>
            <li><a href="/site/contact">Контакты</a></li>
            <li><a href="/site/certificate">Сертификаты</a></li>
        </ul>
    </div>
</div>

</div>
