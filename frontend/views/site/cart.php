<?php

/* @var $this yii\web\View */

use pistol88\cart\widgets\CartInformer;
use pistol88\cart\widgets\ElementsList;
use pistol88\cart\widgets\TruncateButton;
use pistol88\order\widgets\OrderForm;
use yii\helpers\Url;

$this->title = 'Корзина';
?>

<div class="container">

    <ul class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li class="active">Корзина</li>
    </ul>

    <h1>Оформление заказа</h1>

    <div class="cart-page">

<!--        <div class="clearfix remove-add">-->
<!--            <div class="col-sm-1">-->
<!--                <input type="checkbox">-->
<!--            </div>-->
<!--            <div class="col-sm-5">-->
<!--                <button class="bttn-grey"><i class="fa fa-times" aria-hidden="true"></i> Удалить выбранные</button>-->
<!--            </div>-->
<!--            <div class="col-sm-6">-->
<!--                <button class="bttn-grey pull-right">+</button>-->
<!--                <div class="count">-->
<!--                    <div class="pull-right"> шт.</div>-->
<!--                    <a class="plus" href="#">+</a>-->
<!--                    <input class="quantity num" value="1" type="text">-->
<!--                    <a class="minus" href="#">-</a>-->
<!--                </div>-->
<!--                <label>Добавить позицию:</label>-->
<!--                <input type="text">-->
<!--            </div>-->
<!--        </div>-->

    <?=
        ElementsList::widget([
            'type' => 'full',
            'showTotal' => false,
            'showCountArrows' => false,
            'elementView' => '@frontend/views/site/_cartElement',
        ]);
    ?>

    <?php if(!empty(Yii::$app->cart->elements)) { ?>

        <?= CartInformer::widget(['htmlTag' => 'span', 'text' => 'Всего товаров: {c} на сумму {p}']); ?>

        <div class="clearfix">
            <?= TruncateButton::widget([
                'cssClass' => 'bttn-grey',
                'text' => 'Очистить корзину'
            ]); ?>
        </div>

       <p class="h3"><span>Данные пользователя</span></p>

        <?=OrderForm::widget();?>

    <?php } ?>

    </div>

</div>