<?php

/* @var $this yii\web\View */

$this->title = 'Сертификаты';
?>
<div class="container">

    <ul class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li class="active">Сертификаты</li>
    </ul>

    <h1>Сертификаты компании ООО "Чисто-Начисто"</h1>

    <!-- Не дрогаем до запроса клиентом
    <div class="filters">
        <div class="row">
            <div class="col-sm-5  pull-right">Сортировать по: <a href="#">дате добавления</a> <a href="#">наименованию</a></div>
            <div class="col-sm-3">
                <form class="search">
                    <input type="text" placeholder="Поиск по сертификатам">
                    <button>
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </form>
            </div>
            <div class="col-sm-3">
                <label>Раздел:</label>
                <select id="">
                    <option value="">Все</option>
                    <option value="">Группа 1</option>
                    <option value="">Группа 2</option>
                </select>
            </div>
        </div>
    </div>
    -->

    <div class="sertificat">
        <div class="row">

            <?php foreach($certificates as $certificate) { ?>
                <div class="col-sm-4">
                    <div class="item">
                        <a class="img" href="<?=$certificate->image?>" data-fancybox="group" data-caption="<?=$certificate->description?>">
                            <img src="<?=$certificate->image?>" alt="">
                        </a>
                        <p><?=$certificate->description?></p>
                        <a href="<?=$certificate->url?>" target="_blank" class="bttn">Скачать</a>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>

</div>