<?php

/* @var $this yii\web\View */
/* @var object $slider_main */
/* @var object $products_new */
/* @var object $recommended */
/* @var object $news */

use yii\helpers\Url;

$this->title = 'Чисто начисто';
?>
<div class="container-fluid main-slider">
    <div class="container">

        <div id="homeSlider" class="royalSlider rsDefaultInv">
            <?php foreach($slider_main as $slide) { ?>
                <div class="rsContent">
                    <a class="rsImg" href="<?=$slide->image?>"></a>
                    <a class="rsLink" href="<?=$slide->link?>"></a>
                    <div class="rsTmb">
                        <img src="<?=$slide->image_mini?>" class="rs-preview-img">
                        <p><b><?=$slide->text_first?></b></p>
                        <p><em><?=$slide->text_second?></em></p>
                    </div>
                </div>
            <?php }?>
        </div>

    </div>
</div>

<div class="container">
    <div class="block-title"><span>Новинки</span></div>
    <div class="owl-carousel-new">
        <?php foreach($products_new as $product) { ?>
            <div class="item">
                <div class="item-category"><?=$product->category->name?></div>
                <div class="image-block" style="background-image:url(<?=Url::to(['/backend/web'.$product->getImage()->getUrl()])?>);"></div>
                <div class="item-wrapper">
                    <a href="<?=Url::to(['category/product', 'slug' => $product->slug])?>" class="title"><?=$product->getName()?></a>
                    <div class="desc"><?=$product->text?></div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<div class="container-fluid recommend">
    <div class="block-title"><span>Рекомендованные товары</span></div>
    <div class="container">
        <div class="row">
            <?php foreach($recommended as $rec) { ?>
                <div class="col-sm-3">
                    <div class="item">
                        <div class="item-category"><?=$rec->category->name?></div>
                        <div class="image-block" style="background-image:url(<?=Url::to(['/backend/web'.$rec->getImage()->getUrl()])?>);"></div>
                        <a href="<?=Url::to(['category/product', 'slug' => $rec->slug])?>" class="title"><?=$rec->getName()?></a>
                        <div class="desc"><?=$rec->text?></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="container-fluid news">
    <div class="block-title"><span>Новости и статьи</span></div>
    <div class="container">
        <div class="row">
            <?php foreach($news as $new) { ?>
                <div class="col-sm-4">
                    <div class="news-item">
                        <a class="title" href="<?=Url::to(['advertisment/page', 'slug' => $new->slug])?>"><?=$new->title?></a>
                        <div class="desc"><?=$new->anons?></div>
                        <div class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> <?=$new->date?></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
