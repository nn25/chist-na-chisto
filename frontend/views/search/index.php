<?php
use yii\widgets\ActiveForm;
use app\models\Search;
?>

<?php $model = new Search;?>
<?php $form = ActiveForm::begin([
    'action' => '/search/result',
    'options'=>['class' => 'search'],
]); ?>
    <?= $form->field($model, 'search')->label(false)->textInput()?>
    <button type="submit">
        <i class="fa fa-search" aria-hidden="true"></i>
    </button>
<?php ActiveForm::end(); ?>
