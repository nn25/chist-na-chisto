<?php
use yii\helpers\Url;

$this->title = 'Результаты поиска';

?>

<div class="container">

    <ul class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li class="active">Результаты поиска</li>
    </ul>

    <h1>Результаты поиска</h1>

    <div class="row info">
        <div class="col-sm-9">

    <?php foreach($products as $model) { ?>
        <div class="clearfix">
            <div class="item-category"><?=$model->category->name?></div>
            <span class="clearfix"></span>
            <a class="title" href="<?=Url::to(['category/product', 'slug' => $model->slug])?>"><b><?=$model->name?></b></a>
            <a href="<?=Url::to(['category/product', 'slug' => $model->slug])?>" class="info-img">
                <img src="/backend/web<?=$model->getImage()->getUrl()?>" alt="">
            </a>
            <p><?=$model->short_text?></p>
        </div>
    <?php } ?>

        </div>

        <div class="col-sm-3 sidebar">
            <a href="<?=Url::to(['site/foto_video'])?>" class="bttn">Фото и видео</a>
        </div>
    </div>
</div>