<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use pistol88\cart\widgets\CartInformer;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="/frontend/web/image/favicon.png">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<header class="container">
    <div class="row">
        <div class="col-sm-3">
            <a class="logo" href="/"><img src="/frontend/web/image/logo.png" alt=""></a>
        </div>
        <div class="col-sm-6 text-right">
            <?=$this->context->renderPartial('//search/index');?>
        </div>
        <div class="col-sm-3 text-center">
            <a class="cart" href="/site/cart">
                <i class="fa fa-opencart" aria-hidden="true"></i>
                <?= CartInformer::widget(['htmlTag' => 'span', 'text' => 'Товаров: {c}']); ?>
            </a>
        </div>
    </div>
    <ul class="main-menu">
        <li><a href="/">Главная</a></li>
        <li class="parent">
            <a href="<?=Url::to(['category/list'])?>">Продукция <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
            <ul class="submenu">
                <?php
                    $categories = \pistol88\shop\models\Category::buldTree();
                    foreach($categories as $category) {
                ?>
                    <li>
                        <a href="<?=Url::to(['category/list', 'slug' => $category['slug']])?>"><?=$category['name']?></a>
                        <ul class="subsubmenu">
                            <?php foreach($category['childs'] as $cat) { ?>
                                <li>
                                    <a href="<?=Url::to(['category/page', 'slug' => $cat['slug']])?>"><?=$cat['name']?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </li>
        <li><a href="<?=Url::to(['site/foto_video'])?>">Фото и видео</a></li>
        <li><a href="<?=Url::to(['advertisment/list'])?>">Реклама</a></li>
        <li class="parent">
            <a href="<?=Url::to(['site/page', 'slug' => 'obshchaya_informatsiya'])?>">Компания <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
            <ul class="submenu">
                <li><a href="<?=Url::to(['site/page', 'slug' => 'obshchaya_informatsiya'])?>">Общая информация</a></li>
                <li><a href="<?=Url::to(['site/contact'])?>">Контакты</a></li>
                <li><a href="<?=Url::to(['site/certificate'])?>">Сертификаты</a></li>
            </ul>
        </li>
        <li><a href="<?=Url::to(['site/contact'])?>">Контакты</a></li>
    </ul>
</header>

<?php echo Alert::widget();?>
<?=$content?>

<footer class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="phone">+7 (8443) <b>58-48-48</b></div>
                <a href="#" data-toggle="modal" data-target="#messeng"><i class="fa fa-envelope" aria-hidden="true"></i> Написать нам</a>
                <div class="clearfix"></div>
                <p><b>Автохимия и автокосметика для моек</b></p>
                <form>
                    <label for="sendmail">Подпишись на нашу рассылку </label>
                    <input type="text" id="sendmail" placeholder="Ваша электронная почта">
                    <button><i class="fa fa-envelope-o" aria-hidden="true"></i></button>
                </form>
                <p>© 2017 Чисто-Начисто: косметика для чистоты. Все права защищены.</p>
            </div>
            <div class="col-sm-6">
                <ul>
                    <?php foreach($categories as $category) { ?>
                        <li><a href="/category/list/<?=$category['slug']?>"><?=$category['name']?></a></li>
                    <?php } ?>
                    <li><a href="<?=Url::to(['advertisment/list'])?>">Реклама, бренд</a></li>
                    <li><a href="<?=Url::to(['site/certificate'])?>">Сертификаты</a></li>
                </ul>
                <a class="pull-right" href="http://nethammer.ru" target="_blank">Создатель сайта — Nethammer. </a>
            </div>
        </div>
    </div>
</footer>

<!-- Модалка заявка -->
<div class="modal fade" id="messeng" aria-hidden="true">
    <?php
        $modelPopup = new \frontend\models\ContactForm();

        echo $this->context->renderPartial('//mail/index', [
            'model' => $modelPopup
        ]);
    ?>
</div>

<?php $this->endBody() ?>
</body>

<?=$this->registerJs("
    // Слайдер новинки

    $('.owl-carousel-new').owlCarousel({
        navSpeed: 1500,
        dragEndSpeed: 1500,
        nav: false,
        dots: true,
        loop: true,
        autoplay: true,
        items: 4,
        margin: 30,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 4
            }
        }
    });

    // Слайдер на главной

    jQuery(document).ready(function ($) {
        var opts = {
            controlNavigation: 'thumbnails',
            imageScaleMode: 'fill',
            arrowsNav: false,
            arrowsNavHideOnTouch: true,
            fullscreen: false,
            loop: true,
            thumbs: {
                firstMargin: false,
                paddingBottom: 0
            },
            autoPlay: {
                enabled: true,
                pauseOnHover: true
            },
            numImagesToPreload: 3,
            thumbsFirstMargin: false,
            autoScaleSlider: true,
            autoScaleSliderWidth: 960,
            autoScaleSliderHeight: 600,
            keyboardNavEnabled: true,
            navigateByClick: false,
            fadeinLoadedSlide: true,
            imgWidth: 707,
            imgHeight: 397
        };
        ///* if(!$.browser.webkit) {
        opts.imgWidth = 707;
        opts.imgHeight = 397;
        //} */
        var sliderJQ = $('#homeSlider').royalSlider(opts);
    });

    // Галерея в карточке товара

    $('#main-img').elevateZoom({
        gallery:'product-gallery',
        galleryActiveClass: 'active',
        zoomType: 'Inner'
    });
    
    // Плейсхолдеры на форме обратной связи
    
    $(document).ready(function(){
        $('#inputName').attr('placeholder', 'Ваше имя');
        $('#inputEmail').attr('placeholder', 'Email или телефон');
        $('#inputCity').attr('placeholder', 'Город');
        $('#inputBody').attr('placeholder', 'Сообщение');
    });
",yii\web\View::POS_READY);?>

</html>
<?php $this->endPage() ?>
