<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <a href="#" class="fa fa-times pull-right" data-dismiss="modal" aria-hidden="true"></a>Написать нам
        </div>
        <?php $form = ActiveForm::begin([
            'action' => '/mail/',
            'validationUrl' => '/mail/validation',
            'enableAjaxValidation' => true,
            'options'=>['id' => 'contactForm'],
        ]); ?>
            <div class="modal-body">
                <?= $form->field($model, 'name')->label(false)->textInput(['required' => true, 'id' => 'inputName'])?>
                <?= $form->field($model, 'email')->label(false)->textInput(['required' => true, 'id' => 'inputEmail'])?>
                <?= $form->field($model, 'city')->label(false)->textInput(['id' => 'inputCity'])?>
                <?= $form->field($model, 'body')->label(false)->textarea(['id' => 'inputBody'])?>
            </div>
            <div class="modal-footer">
                <?php echo Html::submitButton(Yii::t('frontend', 'Отправить'), ['class' => 'bttn', 'data-toggle' => 'modal']) ?>
<!--                <button type="button" class="bttn" data-toggle="modal">Отправить</button>-->
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>