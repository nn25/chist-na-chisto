<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

$this->title = 'Реклама';
?>

<div class="container">

    <ul class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li class="active">Реклама</li>
    </ul>

    <h1>Реклама, Акции и Новости</h1>

    <div class="row info">
        <div class="col-sm-9">

<?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_one_news',
        'layout' => "<div class='pagination-panel'>{pager}<span>{summary}</span></div>\n
        {items}\n
        <div class='pagination-panel'>{pager}<span>{summary}</span></div>",
        'summary' => '{begin} - {end} из {totalCount} позиций',
        'emptyText' => 'Не найдено записей',
        'pager' => [
            'prevPageLabel' => '< Назад',
            'nextPageLabel' => 'Вперед >',
            'maxButtonCount' => 5,
        ],
    ]);
?>

        </div>

        <div class="col-sm-3 sidebar">
            <a href="<?=Url::to(['site/foto_video'])?>" class="bttn">Фото и видео</a>

            <a href="<?=Url::to(['advertisment/page'])?>" class="image-block">
                <img src="/frontend/web/image/banners/1.jpg" alt="">
            </a>

<!--            <ul class="category-menu">-->
<!--                <li><a href="#">Все</a></li>-->
<!--                <li class="active"><a href="#">Бренд</a></li>-->
<!--                <li><a href="#">Вспомогательная продукция</a></li>-->
<!--                <li><a href="#">Изображения</a></li>-->
<!--            </ul>-->

        </div>
    </div>
</div>
