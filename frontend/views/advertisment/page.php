<?php

/* @var $this yii\web\View */

$this->title = $news->title;
?>

<div class="container">

    <ul class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li><a href="/advertisment/list">Реклама</a></li>
        <li class="active"><?=$news->title?></li>
    </ul>

    <h1><?=$news->title?></h1>

    <div class="row">
        <div class="col-sm-12">
            <div class="static-content">
                <img src="<?=$news->image?>" class="sinle-page-img" alt="">
                <?=$news->text?>
<!--                <a href="#" class="bttn">Скачать</a>-->
            </div>
        </div>
    </div>

    <a href="/advertisment/list" class="return">Назад к списку новостей</a>

</div>
