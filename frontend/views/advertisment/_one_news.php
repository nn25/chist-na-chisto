<?php

use pistol88\cart\widgets\BuyButton;
use yii\helpers\Url;

/* @var object $model */

?>

<div class="clearfix">
    <div class="<?=($model->category=='Бренд')?'item-category':'item-category-o'?>"><?=$model->category?> <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> <?=$model->date?></span></div>
    <span class="clearfix"></span>
    <a class="title" href="<?=Url::to(['advertisment/page', 'slug' => $model->slug])?>"><b><?=$model->title?></b></a>
    <a href="<?=Url::to(['advertisment/page', 'slug' => $model->slug])?>" class="info-img">
        <img src="<?=$model->image?>" alt="">
    </a>
    <p><?=$model->anons?> <a href="<?=Url::to(['advertisment/page', 'slug' => $model->slug])?>">Подробнее ></a></p>
</div>
