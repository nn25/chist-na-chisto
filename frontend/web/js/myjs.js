// Кнопка наверх

$(document).ready(function () {
    $(document.body).append('<a id="back_top" href="#"></a>');
    $('#back_top').hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 120) {
            $('#back_top').fadeIn('slow');
        } else {
            $('#back_top').fadeOut('slow');
        };
    });

    $('#back_top').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        $('#back_top').fadeOut('slow').stop();
    });
});

// Фильтрация для input

$('input.num').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g, '');
});

// Количество в корзине

$('.count .minus').click(function(){
    var data = $(this).closest('.count').find('input').val();
    if (data > 1) {
        $(this).closest('.count').find('input').val(parseInt(data) - 1);
    }
   return false; 
});
$('.count .plus').click(function(){
    var data = $(this).closest('.count').find('input').val();
    $(this).closest('.count').find('input').val(parseInt(data) + 1);
   return false; 
});

// Подставление ссылки на изображение в карточке

$('#product-gallery a').on('click', function(){
    var a = $(this).attr('data-image');
    $('.main-image-box a').attr('href', a);
});

// Выделение всех товаров в корзине

var clickNum = 0;

$('.remove-add input[type=checkbox]').on('click', function(){
    if (clickNum == 0) {
        $('.cart-page input[type=checkbox]').prop('checked', 'checked');
        clickNum = 1;
    } else if (clickNum == 1) {
        $('.cart-page input[type=checkbox]').removeAttr('checked');
        clickNum = 0;
    }
    console.log(clickNum);
    
});