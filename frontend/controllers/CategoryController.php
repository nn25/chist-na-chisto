<?php
namespace frontend\controllers;

use app\models\News;
use pistol88\shop\models\Category;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use pistol88\shop\models\Product;
use Yii;
use app\models\Search;

/**
 * Category controller
 */
class CategoryController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionList($slug = false)
    {
        if($slug == false) {
            $categories = Category::find()->where(['parent_id' => NULL])->all();
        } else {
            $category = Category::find()->where(['slug' => $slug])->one();
            $categories = Category::find()->where(['parent_id' => $category->id])->all();
        }

        if(!empty($categories)) return $this->render('list', ['categories' => $categories]);
        else return $this->redirect('/category/page/'.$slug);
    }

    public function actionPage($slug)
    {
        $category = Category::find()->where(['slug' => $slug])->one();
        $categories = Category::buldTree();

        $news = News::get_random_news();

        $search = new Search();

        if($search->load(Yii::$app->request->post())) {
            $dataProvider = new ActiveDataProvider([
                'query' => Product::find()
                    ->where(['category_id' => $category->id])
                    ->andWhere(['like', 'name', '%'.$search->keyWords.'%', false])
                    ->andWhere(['like', 'code', '%'.$search->sku.'%', false]),
                'pagination' => [
                    'pageSize' => 15,
                ],
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Product::find()->where(['category_id' => $category->id]),
                'pagination' => [
                    'pageSize' => 15,
                ],
            ]);
        }

        return $this->render('page', [
            'category' => $category,
            'categories' => $categories,
            'dataProvider' => $dataProvider,
            'news' => $news
        ]);
    }

    public function actionProduct($slug)
    {
        $product = Product::find()->where(['slug' => $slug])->one();
        $recommended = Product::find()->where(['is_popular' => 'yes'])->orderBy('RAND()')->limit('4')->all();
        return $this->render('product', [
            'product' => $product,
            'recommended' => $recommended,
        ]);
    }

}
