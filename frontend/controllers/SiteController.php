<?php
namespace frontend\controllers;

use app\models\Certificates;
use app\models\Contacts;
use app\models\Page;
use app\models\SliderMain;
use app\models\News;
use app\models\PhotoAndVideo;
use Yii;
use yii\web\Controller;
use pistol88\shop\models\Product;

class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $slider_main = SliderMain::get_all();
        $products_new = Product::find()->where(['is_new' => 'yes'])->all();
        $recommended = Product::find()->where(['is_popular' => 'yes'])->orderBy('RAND()')->limit('4')->all();
        $main_news = News::get_last_news(3);
        return $this->render('index', [
            'slider_main' => $slider_main,
            'products_new' => $products_new,
            'recommended' => $recommended,
            'news' => $main_news
        ]);
    }

    public function actionFoto_video()
    {
        $content = PhotoAndVideo::get_all();
        return $this->render('foto_video', ['content' => $content]);
    }

    public function actionContact()
    {
        $contacts = Contacts::get_all();
        $title = Contacts::get_title();
        return $this->render('contact', ['contacts' => $contacts, 'title' => $title]);
    }

    public function actionPage($slug)
    {
        $content = Page::get_page_by_slug($slug);
        return $this->render('page', ['content' => $content]);
    }

    public function actionCertificate()
    {
        $certificates = Certificates::get_all();
        return $this->render('certificate', ['certificates' => $certificates]);
    }

    public function actionCart()
    {
        $elements = Yii::$app->cart->elements;
        return $this->render('cart', [
            'elements' => $elements
        ]);
    }

    public function actionThanks()
    {
        return $this->render('thanks');
    }

}
