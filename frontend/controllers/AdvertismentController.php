<?php
namespace frontend\controllers;

use app\models\News;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class AdvertismentController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionList()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find()->where(['published' => 1])->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPage($slug)
    {
        $page=News::get_news_by_slug($slug);
        return $this->render('page', ['news' => $page]);
    }

}
