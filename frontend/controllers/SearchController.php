<?php
namespace frontend\controllers;

use pistol88\shop\models\Product;
use Yii;
use yii\web\Controller;
use app\models\Search;

/**
 * Search controller
 */
class SearchController extends Controller
{
    public function behaviors()
    {
        return [];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionResult()
    {
        if(Yii::$app->request->post())
        {
            $products = Product::find()->where(['like', 'name', '%'.$_POST['Search']['search'].'%', false])->all();
            return $this->render('result', ['products' => $products]);
        }
        else return $this->render(['index']);
    }

}
