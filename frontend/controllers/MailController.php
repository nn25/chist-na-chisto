<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\ContactForm;

/**
 * Mail controller
 */
class MailController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ContactForm();
        if($model->load(Yii::$app->request->post())) {
            if($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Ваше сообщение успешно отправлено');
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->render('index');
    }

    public function actionValidation(){
        $model = new ContactForm();
        if($model->load(Yii::$app->request->post())) {
            return true;
        }
        return false;
    }
}
