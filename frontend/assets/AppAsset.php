<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Open+Sans',
        '//fonts.googleapis.com/css?family=Open+Sans:700',
        'js/assets/owl.carousel.css',
        'js/assets/owl.theme.default.css',
        'css/font-awesome.css',
        'js/fancybox/jquery.fancybox.css',
        'js/RS/royalslider.css',
        'js/RS/rs-default.css',
        'css/style.css',
        'css/media.css',
        'css/nn25.css',
    ];
    public $js = [
        'js/fancybox/jquery.elevatezoom.js',
        'js/fancybox/jquery.fancybox.js',
        'js/owl.carousel.js',
        'js/myjs.js',
        'js/RS/jquery.royalslider.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
